import React from 'react'

const Header = () => {
     return (
          <div>
                <nav className="navbar">
          <div className="max-width">
               <div className="logo">
                    <a href="#">Munar<span>bek</span></a></div>
               <ul className="menu">
                    <li><a href="#home" className="menu-btn">Главная</a></li>
                    <li><a href="#about" className="menu-btn">Обо мне</a></li>
                    <li><a href="#services" className="menu-btn">Что я умею</a></li>
                    <li><a href="#skills" className="menu-btn">Навыки</a></li>
                    <li><a href="#contact" className="menu-btn">Контакты</a></li>
               </ul>
               <div className="menu-btn">
                    <i className="fas fa-bars"></i>
               </div>
          </div>
     </nav>


     <section className="home" id="home">
          <div className="max-width">
               <div className="home-content">
                    <div className="text-1">
                         Привет, меня зовут</div>
                    <div className="text-2">
                         Мунарбек</div>
                    <div className="text-3">
                         И, я <span className="typing"></span></div>
                    <a href="https://career.habr.com/munar-cymbatov">Немного о мне</a>
               </div>
          </div>
     </section>

          </div>
     )
}

export default Header

