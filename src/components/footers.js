import React from 'react'

const footers = () => {
     return (
     
               <div className="skills" id="skills">
          <div className="max-width">
               <h2 className="title">
                    My skills</h2>
               <div className="skills-content">
                    <div className="column left">
                         <div className="text">
                              My creative skills &experiences.</div>
                         <ul>
                              <li>Опыт работы JavaScript, ES6+, ООП, Классы , Promise</li>

                              <li>опыт работы с React</li>

                              <li>Навыки работы Webpack, и с пакетными менеджерами </li>

                              <li>Навыки работы с Vue.js</li>

                              <li>Работа с REST API, AJAX;</li>

                              <li>Понимание HTTP-протоколы, TCP/IP;</li>

                              <li>Уверенные знания HTML5, CSS3 (sass, scss);</li>

                              <li> Опыт работы в команде и знание Git.</li>

                         </ul>
                         <a href="#">Read more</a>
                    </div>
                    <div className="column right">
                         <div className="bars">
                              <div className="info">
                                   <span>HTML</span>
                                   <span>90%</span>
                              </div>
                              <div className="line html">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>CSS</span>
                                   <span>77%</span>
                              </div>
                              <div className="line css">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>JavaScript ES6+</span>
                                   <span>48%</span>
                              </div>
                              <div className="line js">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>Node Js</span>
                                   <span>20%</span>
                              </div>
                              <div className="line node">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>SASS/SCSS</span>
                                   <span>70%</span>
                              </div>
                              <div className="line php">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>Git</span>
                                   <span>40%</span>
                              </div>
                              <div className="line mysql">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>BEM</span>
                                   <span>40%</span>
                              </div>
                              <div className="line bem">
                              </div>
                         </div>
                         <div className="bars">
                              <div className="info">
                                   <span>REACT/REDUX</span>
                                   <span>50%</span>
                              </div>
                              <div className="line react">
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          </div>
     )
}

export default footers

